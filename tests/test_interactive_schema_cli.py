# mandar un error
# validar widget obtenido con el schema del error
# obtener el widget indicado
# renderizar widget
# interactuar con widget
# validar resultado

import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLI import JSONSchemaCLI
from .helpers.testing_helpers import make_multiple_inputs

def test_cliSetStringValue(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {"dummy": "dummy"}
    expected_instance = {"dummy": "dummy", "str": "im-a-string"}
        
    path = 'str'
    value = 'im-a-string'
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
        
    cli = JSONSchemaCLI(schema, instance)
    cli.modify()
    
    assert cli.instance == expected_instance

def test_cliSetIntegerValue(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {"dummy": "dummy"}
    expected_instance = {"dummy": "dummy", "num": 2}
        
    path = 'num'
    value = '2'
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
        
    cli = JSONSchemaCLI(schema, instance)
    cli.modify()
    
    assert cli.instance == expected_instance
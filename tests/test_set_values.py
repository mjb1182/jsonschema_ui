import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLIWidget import JSONSchemaCLIWidget

@pytest.fixture
def schema():
    with open('json/set_values/set_values.schema.json', 'r') as file:
        schema = json.loads(file.read())
    return schema

def test_setString(schema):
    instance = {'dummy': 'dummy'}
    path, value = ['str'], 'im-a-string'
    expected_instance = {'dummy': 'dummy', 'str': value}
    
    ob = JSONObject(schema, instance)
    ob.set_value(path, value)
    
    assert expected_instance == ob.instance
    
def test_setInvalidString(schema):
    instance = {'dummy': 'dummy'}
    path, value = ['str'], 2
    expected_instance = {'dummy': 'dummy', 'str': value}
    
    ob = JSONObject(schema, instance)
    ob.set_value(path, value)
    
    assert expected_instance == ob.instance
    
def test_setInteger(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['num'], 2

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'num': value}
    
def test_setInvalidInteger(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['num'], 'im-an-invalid-integer'

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'num': value}
    
def test_setEmptyObject(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['ob'], {}

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'ob': value}
    
def test_setObject(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['ob'], {'str': 'im-a-valid-string'}

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'ob': value}
    
def test_setInvalidObject(schema):
    instance = {'dummy': 'dummy'}
    path, value = ['ob'], {'num': 'im-a-invalid-number'}
    expected_instance = {'dummy': 'dummy', 'ob': value}

    ob = JSONObject(schema, instance)
    ob.set_value(path, value)
    assert ob.instance == expected_instance
    
def test_setObjectProperty(schema):
    instance = {'dummy': 'dummy', 'ob': {'prop1': 'v1'}}
    path, value = ['ob', 'prop2'], 'v2'
    expected_instance = {'dummy': 'dummy', 'ob': {'prop1': 'v1', 'prop2': 'v2'}}
    
    ob = JSONObject(schema, instance)
    ob.set_value(path, value)
    assert ob.instance == expected_instance
    
    
def test_setEmptyArray(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['arr'], []

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'arr': value}

def test_setArray(schema):
    instance = {'dummy': 'dummy'}
    ob = JSONObject(schema, instance)
    path, value = ['arr'], ['el']

    ob.set_value(path, value)
    assert ob.instance == {'dummy': 'dummy', 'arr': value}
    
def test_appendToArray(schema):
    instance = {'dummy': 'dummy', 'arr': ['a']}
    path, value = ['arr'], 'b'
    expected_instance = {'dummy': 'dummy', 'arr': ['a', 'b']}
    
    ob = JSONObject(schema, instance)
    
    ob.append_value(path, value)
    assert ob.instance == expected_instance

def test_appendToNestedArray(schema):
    instance = {'dummy': 'dummy', 'arr': ['a', ['b']]}
    path, value = ['arr', 1], 'c'
    expected_instance = instance = {'dummy': 'dummy', 'arr': ['a', ['b', 'c']]}
    
    ob = JSONObject(schema, instance)
    
    ob.append_value(path, value)
    assert ob.instance == expected_instance
    
def test_appendToNonExistentArray(schema):
    instance = {'dummy': 'dummy', 'arr': ['a']}
    path, value = ['arr2'], 'b'
    expected_instance = {'dummy': 'dummy', 'arr': ['a'], 'arr2': ['b']}
    
    ob = JSONObject(schema, instance)
    
    ob.append_value(path, value)
    assert ob.instance == expected_instance
    
def test_replaceElementInArray(schema):
    instance = {"dummy": "dummy", 'arr': ['a', 'replace_me', 'c']}
    path, value = ['arr', 1], 'b'
    expected_instance = {"dummy": "dummy", 'arr': ['a', 'b', 'c']}
    
    ob = JSONObject(schema, instance)
    
    ob.set_value(path, value)
    assert ob.instance == expected_instance
    
def test_removeString(schema):
    instance = {"dummy": "dummy", 'str': 'im-a-string'}
    expected_instance = {"dummy": "dummy"}
    path = ['str']
    
    ob = JSONObject(schema, instance)
    
    ob.remove_value(path)
    assert ob.instance == expected_instance
    
def test_removeObject(schema):
    instance = {"dummy": "dummy", 'ob': {'str': 'im-a-string'}}
    expected_instance = {"dummy": "dummy"}
    path = ['ob']
    
    ob = JSONObject(schema, instance)
    
    ob.remove_value(path)
    assert ob.instance == expected_instance
    
def test_removeElementInArray(schema):
    instance = {"dummy": "dummy", 'arr': ['a', 'b', 'c']}
    expected_instance = {"dummy": "dummy", 'arr': ['a', 'c']}
    path = ['arr', 1]
    
    ob = JSONObject(schema, instance)
    
    ob.remove_value(path)
    assert ob.instance == expected_instance
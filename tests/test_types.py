import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

def test_stringTypeSuccess():
    schema_file = 'json/types/basic_types.schema.json'
    schema, _ = get_schema_and_instance_from_file(schema_file)
    instance = {"str": "s"}
    
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()
    
def test_stringTypeError():
    schema_file = 'json/types/basic_types.schema.json'
    schema, _ = get_schema_and_instance_from_file(schema_file)
    instance = {"str": 2}
    
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
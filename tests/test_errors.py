import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

"""
if pytest.__version__ < "3.0.0":
  pytest.skip()
else:
  pytestmark = pytest.mark.skip
"""

def test_stringTypeError():
    schema_file = 'json/types/basic_types.schema.json'
    schema, _ = get_schema_and_instance_from_file(schema_file)
    instance = {"str": 2}
    
    ob = JSONObject(schema=schema, instance=instance)
    errors = ob.get_errors()
    error = next(errors)
    
    assert error.instance == 2
    assert error.schema == {'type': 'string'}
    assert error.validator == 'type'
    assert error.validator_value == 'string'
    
    assert list(error.path) == ['str']
    assert list(error.relative_path) == ['str']
    assert list(error.absolute_path) == ['str']
    
    assert list(error.schema_path) == ['properties', 'str', 'type']
    assert list(error.relative_schema_path) == ['properties', 'str', 'type']
    assert list(error.absolute_schema_path) == ['properties', 'str', 'type']

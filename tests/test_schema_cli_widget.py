import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLIWidget import JSONSchemaCLIWidget
from .helpers.testing_helpers import make_multiple_inputs

def test_stringInput(monkeypatch):
    input_value = "1"
    monkeypatch.setattr('builtins.input', lambda x: input_value)
    schema = json.loads('{"type": "string"}')
    cli = JSONSchemaCLIWidget(schema=schema)
    value = cli.render()
    assert type(value) == str
    assert value == input_value

def test_integerInput(monkeypatch):
    input_value = "1"
    monkeypatch.setattr('builtins.input', lambda x: input_value)
    schema = json.loads('{"type": "integer"}')
    cli = JSONSchemaCLIWidget(schema=schema)
    value = cli.render()
    assert type(value) == int
    assert value == int(input_value)

def test_wrongIntegerInputRaises(monkeypatch):
    input_value = "im-an-string"
    monkeypatch.setattr('builtins.input', lambda x: input_value)
    schema = json.loads('{"type": "integer"}')
    cli = JSONSchemaCLIWidget(schema=schema)
    with pytest.raises(ValueError):
        cli.render()
        
def test_retryWrongIntegerInput(monkeypatch):
    # monkeypatch.setattr('builtins.input', inputs(g))
    inputs = make_multiple_inputs(["im-an-string", "1"])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    schema = json.loads('{"type": "integer"}')
    cli = JSONSchemaCLIWidget(schema=schema)
    value = cli.render(on_error='retry')
    assert type(value) == int
    assert value == int("1")
        
        

def _test_stringTypeError():
    schema_file = 'json/types/basic_types.schema.json'
    schema, _ = get_schema_and_instance_from_file(schema_file)
    instance = {"str": 2}
    
    ob = JSONObject(schema=schema, instance=instance)
    errors = ob.get_errors()
    error = next(errors)
    
    assert error.instance == 2
    assert error.schema == {'type': 'string'}
    assert error.validator == 'type'
    assert error.validator_value == 'string'
    
    assert list(error.path) == ['str']
    assert list(error.relative_path) == ['str']
    assert list(error.absolute_path) == ['str']
    
    assert list(error.schema_path) == ['properties', 'str', 'type']
    assert list(error.relative_schema_path) == ['properties', 'str', 'type']
    assert list(error.absolute_schema_path) == ['properties', 'str', 'type']
    
    
def _test_something_that_involves_user_input(monkeypatch):

    # monkeypatch the "input" function, so that it returns "Mark".
    # This simulates the user entering "Mark" in the terminal:
    monkeypatch.setattr('builtins.input', lambda x: "Mark")

    # go about using input() like you normally would:
    i = input("What is your name?")
    assert i == "Mark"
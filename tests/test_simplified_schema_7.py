import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

def test_simpleSimpliedSucceedSchema7():
    with open('json/schema_7/updated_schema/simple_if.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if__simplified_succeed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if__condition_succeed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_simpleSimpliedFailedSchema7():
    with open('json/schema_7/updated_schema/simple_if.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if__simplified_failed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if__condition_failed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_simpleSimpliedFailedWithElseSchema7():
    with open('json/schema_7/updated_schema/simple_if_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if_else__simplified_failed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/simple_if__condition_failed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l1SimpliedSucceedSchema7():
    with open('json/schema_7/updated_schema/l2_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_if_then_else__simplified_succeed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_if_then_else__condition_succeed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l1SimpliedFailedSchema7():
    with open('json/schema_7/updated_schema/l2_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_if_then_else__simplified_failed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_if_then_else__condition_failed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l2SimplifiedThenSucceedSchema7():
    with open('json/schema_7/updated_schema/l2_double_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__simplified__then_succeed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__then_succeed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l2SimplifiedThenFailedSchema7():
    with open('json/schema_7/updated_schema/l2_double_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__simplified__then_failed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__then_failed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l2SimplifiedElseSucceedSchema7():
    with open('json/schema_7/updated_schema/l2_double_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__simplified__else_succeed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__else_succeed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
    
def test_l2SimplifiedElseFailedSchema7():
    with open('json/schema_7/updated_schema/l2_double_if_then_else.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__simplified__else_failed.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    with open('json/schema_7/updated_schema/l2_double_if_then_else__else_failed.json', 'r') as file:
        instance = json.loads(file.read())
        
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
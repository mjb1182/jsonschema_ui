import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

"""
if pytest.__version__ < "3.0.0":
  pytest.skip()
else:
  pytestmark = pytest.mark.skip
"""  
  
def test_canCreateObjectAndValidate():
    ob = JSONObject(schema={}, instance={})
    assert ob.is_valid()

def test_validSimpleSchema6():
    schema_file = 'json/schema_6/simple.schema.json'
    instance_file = 'json/schema_6/simple__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()

def test_invalidSimpleSchema6():
    schema_file = 'json/schema_6/simple.schema.json'
    schema, _ = get_schema_and_instance_from_file(schema_file)
    
    # missing required
    instance = {}
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
    # wrong string
    instance = {"im_required": "val"}
    instance['im_string'] = 1
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
    # wrong list
    instance = {"im_required": "val"}
    instance['im_string_array'] = {}
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
    # wrong object
    instance = {"im_required": "val"}
    instance['im_object'] = "val"
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
def test_validNestedSchema6():
    schema_file = 'json/schema_6/nested.schema.json'
    instance_file = 'json/schema_6/nested__valiejson'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()

def test_invalidNestedSchema6():
    schema_file = 'json/schema_6/nested.schema.json'
    instance_file = 'json/schema_6/simple_allof__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()        
    
def test_validComplexNestedSchema6():
    schema_file = 'json/schema_6/complex_nested.schema.json'
    instance_file = 'json/schema_6/complex_nested__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()   
    
def test_invalidComplexNestedSchema6():
    schema_file = 'json/schema_6/complex_nested.schema.json'
    instance_file = 'json/schema_6/complex_nested__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()

def test_validDoubleComplexNestedSchema6():
    schema_file = 'json/schema_6/double_complex_nested.schema.json'
    instance_file = 'json/schema_6/double_complex_nested__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()

def test_invalidDoubleComplexNestedSchema6():
    schema_file = 'json/schema_6/double_complex_nested.schema.json'
    instance_file = 'json/schema_6/double_complex_nested__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
def test_validSimpleAllOfSchema6():
    schema_file = 'json/schema_6/simple_allof.schema.json'
    instance_file = 'json/schema_6/simple_allof__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    print(20*"*")
    print(20*"*")
    print(ob.instance)
    for e in ob.get_errors():
        print('error: ', e.message)
    assert ob.is_valid()

def test_invalidSimpleAllOfSchema6():
    schema_file = 'json/schema_6/simple_allof.schema.json'
    instance_file = 'json/schema_6/simple_allof__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()
    
def test_validAllOfNestedRef():
    schema_file = 'json/schema_6/allof_nested_ref.schema.json'
    instance_file = 'json/schema_6/allof_nested_ref__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()

def test_invalidAllOfNestedRef():
    schema_file = 'json/schema_6/allof_nested_ref.schema.json'
    instance_file = 'json/schema_6/allof_nested_ref__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()

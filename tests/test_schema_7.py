import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

def test_validSimpleSchema7():
    schema_file = 'json/schema_7/simple_if.schema.json'
    instance_file = 'json/schema_7/simple_if__valid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert ob.is_valid()

def test_invalidSimpleSchema7():
    schema_file = 'json/schema_7/simple_if.schema.json'
    instance_file = 'json/schema_7/simple_if__invalid.json'
    schema, instance = get_schema_and_instance_from_file(schema_file, instance_file)
    ob = JSONObject(schema=schema, instance=instance)
    assert not ob.is_valid()

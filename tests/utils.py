import json

def get_schema_and_instance_from_file(schema_file=None, instance_file=None):
    schema = None
    instance = None
    if schema_file:
        with open(schema_file, "r") as file:
            schema = json.loads(file.read())
    if instance_file:
        with open(instance_file, "r") as file:
            instance = json.loads(file.read())
    return schema, instance
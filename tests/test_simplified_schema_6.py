import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject

def test_simpleRefSchema6():
    with open('json/schema_6/updated_schema/simple_ref.schema.json', 'r') as file:
        schema = json.loads(file.read())
    with open('json/schema_6/updated_schema/simple_ref_simplified.schema.json', 'r') as file:
        schema_simplified = json.loads(file.read())
    instance = {}
    
    ob = JSONObject(schema=schema, instance=instance)
    assert schema_simplified == ob.get_updated_schema()
import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLIWidget import JSONSchemaCLIWidget
from .jsonschema_ui.JSONSchemaCLI import JSONSchemaCLI
from .helpers.testing_helpers import make_multiple_inputs

"""
- instancion la interfaz
- le indico que quiero cambiar un valor en un path
- la niterfaz obtiene el schema para ese path
- crea el widget indicado
- pide el valor con el widget
- valida la entrada
    - si es valida ingresa la el valor
    - si el erroneo vuelve a pedirlo
"""

def test_modifySimpleString(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'str'
    value = 'im-a-string'
    expected_instance = {'dummy': 'dummy', 'str': 'im-a-string'}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_modifySimpleInteger(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'num'
    value = 2
    expected_instance = {'dummy': 'dummy', 'num': 2}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_modifySimpleIntegerRetry(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'num'
    value = 2
    expected_instance = {'dummy': 'dummy', 'num': 2}
    
    inputs = make_multiple_inputs([path, 'im-an-invalid-integer', value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_addEmptyArray(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'arr'
    value = '[]'
    expected_instance = {'dummy': 'dummy', 'arr': []}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_addNotEmptyArray(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'arr'
    value = '["a1", "a2"]'
    expected_instance = {'dummy': 'dummy', 'arr': ['a1', 'a2']}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_addReplaceNotEmptyArray(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy', 'arr': ['a1', 'a2']}
    path = 'arr'
    value = '["r1", "r2"]'
    expected_instance = {'dummy': 'dummy', 'arr': ['r1', 'r2']}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_addEmptyObject(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'ob-dummy'
    value = '{}'
    expected_instance = {'dummy': 'dummy', 'ob-dummy': {}}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_addObject(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy'}
    path = 'ob-dummy'
    value = '{"a": "a"}'
    expected_instance = {'dummy': 'dummy', 'ob-dummy': {'a': 'a'}}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
    
def test_replaceObject(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy', 'ob-dummy': {'a': 'a'}}
    path = 'ob-dummy'
    value = '{"r": "r"}'
    expected_instance = {'dummy': 'dummy', 'ob-dummy': {'r': 'r'}}
    
    inputs = make_multiple_inputs([path, value])
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.modify()
    
    assert expected_instance == cli.instance
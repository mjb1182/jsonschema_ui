import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLI import JSONSchemaCLI
from .helpers.testing_helpers import make_multiple_inputs


def test_complex1(monkeypatch):
    
    with open('json/complex/schema6.schema.json', 'r') as file:
        schema = json.loads(file.read())
    
    instance = {}
    instance['im-required'] = 'im-required'
    instance['model'] = 'model'
    instance['ds'] = {'path': 'path', 'steps': [{'op': 'op'}]}
    
    input_values = ['ds.steps.0.params', '{"p1": "p1", "p2": "p2"}']
    
    inputs = make_multiple_inputs(input_values)
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)
    cli.fix_errors()
    
    assert cli.ob.is_valid()

def test_simpleIf(monkeypatch):
    
    with open('json/complex/simple-if.schema.json', 'r') as file:
        schema = json.loads(file.read())
    
    instance = {}
    instance['p'] = '1'
        
    input_values = ['p1', 'string', '1']
    
    inputs = make_multiple_inputs(input_values)
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)
    cli.fix_errors()
    
    assert cli.ob.is_valid()

# TODO: Make this test work

def test_wellStructuredIf(monkeypatch):
    
    with open('json/complex/well-structured-if.schema.json', 'r') as file:
        schema = json.loads(file.read())
    
    instance = {}
    instance['p'] = '1'
    
    input_values = ['p1', 'string', '1']
    
    inputs = make_multiple_inputs(input_values)
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)
    cli.fix_errors()
    
    assert cli.ob.is_valid()

import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLIWidget import JSONSchemaCLIWidget
from .jsonschema_ui.JSONSchemaCLI import JSONSchemaCLI
from .helpers.testing_helpers import make_multiple_inputs


@pytest.mark.skip
def test_fixWrongString(monkeypatch):
    with open('json/cli/schema.json', 'r') as file:
        schema = json.loads(file.read())
    instance = {'dummy': 'dummy', 'str': 2}
    expected_instance = {'dummy': 'dummy', 'str': 'im-a-string'}
    
    input_values = []
    # choose path
    input_values.append('str')
    # set value for 'str'
    input_values.append('im-a-string')
    
    inputs = make_multiple_inputs(input_values)
    monkeypatch.setitem(__builtins__, 'input', inputs)
    
    cli = JSONSchemaCLI(schema, instance)    
    cli.fix()
    
    assert cli.instance == expected_instance
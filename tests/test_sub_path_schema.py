import json, pytest
from .utils import get_schema_and_instance_from_file
from .jsonschema_ui.JSONObject import JSONObject
from .jsonschema_ui.JSONSchemaCLIWidget import JSONSchemaCLIWidget
from .helpers.testing_helpers import make_multiple_inputs

def test_getStringSubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {'type': 'string'}
    instance = {}
    path = ['str']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema
    

    
def test_getNestedStringSubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {'type': 'string'}
    instance = {}
    path = ['ob', 'str']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema
    
def test_getNonExistentStringSubschema():
    with open('./json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {}
    instance = {}
    path = ['str-non-existent']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema
    
def test_getNonExistentNestedStringSubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {}
    instance = {}
    path = ['ob', 'str-non-existent']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema

def test_getArrayDummytSubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {'type': 'array'}
    instance = {}
    path = ['arr-dummy']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema

def test_getNestedStringInArraySubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {'type': 'string'}
    instance = {}
    path = ['arr-str', '']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema

def test_getNestedStringInNestedArraySubschema():
    with open('json/sub_schema/schema.json') as file:
        schema = json.loads(file.read())
    expected_sub_schema = {'type': 'string'}
    instance = {}
    path = ['arr-arr-str','','']
    
    ob = JSONObject(schema, instance)
    sub_schema = ob.get_subschema(path)
    
    assert sub_schema == expected_sub_schema
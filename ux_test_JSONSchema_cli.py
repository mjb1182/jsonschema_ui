"""
import json, pytest
from tests.utils import get_schema_and_instance_from_file
from jsonschema_ui.JSONObject import JSONObject
from jsonschema_ui.JSONSchemaCLI import JSONSchemaCLI
from helpers.testing_helpers import make_multiple_inputs
from jsonschema_ui.utils import json_find_items_by_key_generator

from IPython.core.debugger import Pdb
# Pdb().set_trace()


with open('tests/json/complex/well-structured-if.schema.json', 'r') as file:
    schema = json.loads(file.read())

instance = {}
instance['p'] = '2'
instance['p1'] = '2'

cli = JSONSchemaCLI(schema, instance)
#cli.fix_errors()

new_schema = cli.ob.get_updated_schema()
new_schema
"""
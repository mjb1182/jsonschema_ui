import copy 
from .JSONObject import JSONObject
from .JSONSchemaCLIWidget import JSONSchemaCLIWidget
from helpers.cli_helpers import cli_choose_index_option, cli_choose_option

from IPython.core.debugger import Pdb
# Pdb().set_trace()

class JSONSchemaCLI:
    def __init__(self, schema, instance, schema_path='./'):
        self.instance = instance
        self.schema = schema
        self.ob = JSONObject(schema, instance, schema_path=schema_path)
        self.schema_path = schema_path
    def modify(self):
        # TODO: Move retries management from widget to this manager
        print('Modify Instance')
        path = input('Enter path: ').split('.')
        
        # TODO: Consider the posibility that a number could actually be a string or a property in a object
        path_with_integers = []
        for p in path:
            # PATCH: path can contain integer as index to arrays
            if p.isnumeric():
                path_with_integers.append(int(p))
            else:
                path_with_integers.append(p)
        
        print('path array: ', path_with_integers)
        schema = self.ob.get_subschema(path_with_integers)
        
        # Just in case create a copy
        # TODO: Check if it's necessary to create a copy
        schema = copy.deepcopy(schema)
        if 'type' not in schema.keys():
            json_types = ['string', 'number', 'object', 'array']
            schema['type'] = cli_choose_option(json_types, 'Choose a type')            
        
        
        widget = JSONSchemaCLIWidget(schema)
        value = widget.render()
        self.ob.set_value(path, value)
        
    def fix_errors(self):
        while True:
            if self.ob.is_valid():
                print('No errors to fix')
                break
            else:
                print('instance: ', self.instance)
                print(20*"=")
                for error in self.ob.get_errors():
                    msg = 'path: %s || msg: %s' % (error.schema_path, error.message)
                    print(msg)
                self.modify()
                

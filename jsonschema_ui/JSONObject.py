from jsonschema import Draft6Validator
from .utils import json_find_items_by_key_generator, json_pop_nested_item
from .constants import SCHEMA_RESERVED_WORDS
import copy
import dpath
import json
from dpath.util import MERGE_REPLACE, MERGE_ADDITIVE, MERGE_TYPESAFE
from helpers.json_helpers import json_set_nested_value

from IPython.core.debugger import Pdb
# Pdb().set_trace()

MERGE_POLICIES = {
        'replace': MERGE_REPLACE,
        'add': MERGE_ADDITIVE,
        'safe': MERGE_TYPESAFE
        }

class JSONObject:
    def __init__(self, schema, instance, schema_path='./'):
        self.schema = schema
        self.instance = instance
        self.schema_path = schema_path
    
    def remove_value(self, path):
        dpath.delete(self.instance, path)
    
    def set_value(self, path, value):
        dpath.new(self.instance, path, value)
        
    def append_value(self, path, value):
        arr = []
        try:
            arr = dpath.get(self.instance, path)
        except KeyError:
            dpath.new(self.instance, path, arr)
        arr.append(value)
        
    def is_valid(self, schema=None, instance=None):
        if not schema:
            schema = self.schema
        if not instance:
            instance = self.instance
        schema = self.get_updated_schema(schema=schema)
        v = Draft6Validator(schema)
        return v.is_valid(instance)
    
    def get_errors(self):
        current_schema = self.get_updated_schema()
        v = Draft6Validator(current_schema)
        return v.iter_errors(self.instance)
    
    def get_updated_schema(self, schema=None):
        schema = self._get_updated_schema(schema)
        
        # remove definitions
        try:
            schema.pop('definitions')
        except KeyError:
            pass
        
        return schema
    
    def _get_updated_schema(self, schema=None):
        if schema == None:
            schema = copy.deepcopy(self.schema)
        else:
            schema = copy.deepcopy(schema)
        
        if True:
            # Replace $ref's
            ref_generator = json_find_items_by_key_generator(schema, '$ref')
            
            try:
                ref_value, prop_key, ref_prop_path = next(ref_generator)
                
                if ref_value.startswith('file:'):
                    file_path = ref_value.replace('file:', '')
                    file_path = self.schema_path + '/' + file_path
                    
                    with open(file_path, 'r') as file:
                        ref_schema = json.loads(file.read())
                else:
                    # Get path and remove first "#"
                    ref_path = ref_value.split('/')[1:]
                    # get referenced schema
                    ref_schema = dpath.get(schema, ref_path)
                
                # remove ref property
                #json_pop_nested_item(schema, ref_prop_path[:-1])
                #Pdb().set_trace()
                path = ref_prop_path[:-1]
                ref_obj = dpath.get(schema, path)
                ref_obj.pop('$ref')
                
                # Create temp schema
                #ref_schema_extra = {}
                #dpath.new(ref_schema_extra, ref_prop_path[:-1], sub_schema)
                
                #dpath.set(sub_schema, ref_prop_path[:-1], sub_schema)
                # tmp_schema = json_set_nested_value(ref_schema, ref_prop_path, ref_schema_extra)
                #tmp_schema = json_set_nested_value(ref_schema_extra, ref_prop_path[:-1], ref_schema)
                
                # merge schemas
                #dpath.merge(schema, tmp_schema)
                schema = json_set_nested_value(schema, ref_prop_path[:-1], ref_schema)
                
                schema = self.get_updated_schema(schema)
                
            except StopIteration:
                pass
            
        # Replace if's
        if_generator = json_find_items_by_key_generator(schema, 'if')
        
        try:
            sub_schema_cond, x, sub_schema_path = next(if_generator)
            
            condition = json_pop_nested_item(schema, sub_schema_path)
            v = Draft6Validator(condition)
            
            # get sub instance corresponding to the condition
            sub_inst_path = []
            parent = None
            for p in sub_schema_path:
                # only those inmediately after a property can be part of the path
                if parent == 'properties':
                    sub_inst_path.append(p)
                parent = p
            if sub_inst_path:
                cond_instance = dpath.get(self.instance, sub_inst_path)
            else:
                cond_instance = self.instance
            
            value_key = 'then' if v.is_valid(cond_instance) else 'else'
            
            try:
                schema_extra_value = json_pop_nested_item(schema, sub_schema_path[:-1] + [value_key])
            except KeyError:
                schema_extra_value = {}

            # Remove if terms
            for t in ['then', 'else']:
                try:
                    json_pop_nested_item(schema, sub_schema_path[:-1] + [t])
                except KeyError:
                    pass       
            
            schema_extra = {}
            #if len(sub_schema_path) > 1:
                # dpath.new(schema_extra, sub_schema_path[:-1], schema_extra_value)
            #    schema_extra = json_set_nested_value(schema_extra, sub_schema_path[:-1], schema_extra_value)
            #else:
            #    schema_extra = schema_extra_value
            #dpath.merge(schema, schema_extra, flags=MERGE_POLICIES['add'])
            schema = json_set_nested_value(schema, sub_schema_path[:-1], schema_extra_value)
            schema = self.get_updated_schema(schema)
        except StopIteration:
            pass
        return schema
        
    def get_subschema(self, path):
        schema = self.get_updated_schema()
        
        schema_path = []
        for p in path:
            if p != '' and type(p) != int:
                schema_path += ['properties', p]
            else:
                schema_path += ['items']
        try:
            sub_schema = dpath.get(schema, schema_path)
        except KeyError as e:
            sub_schema = {}
        return sub_schema

    

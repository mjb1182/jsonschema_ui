import copy

def json_find_items_by_key_generator(json_input_ref, lookup_key, path=[]):
    json_input = copy.deepcopy(json_input_ref)
    skip_keys = ['definitions']
    if isinstance(json_input, dict):
        index = 0
        for k, v in json_input.items():
            if k in skip_keys:
                continue
            if k == lookup_key:
                path = path + [k]
                yield v, k, path
            else:
                tmp_path = path + [k]
                yield from json_find_items_by_key_generator(v, lookup_key, tmp_path)
            index += 1
    elif isinstance(json_input, list):
        index = 0
        for idx, item in enumerate(json_input):
            yield from json_find_items_by_key_generator(item, lookup_key, path + [idx])
            
def json_pop_nested_item(instance, path=[]):
    val = None
    if len(path) == 1:
        val = instance.pop(path[0])
    else:
        item = instance
        for k in path[:-1]:
            if type(k) == int:
                item = item[k]
            else:
                item = item.get(k)
        val = item.pop(path[-1])
    return val

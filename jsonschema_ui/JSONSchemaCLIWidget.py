from jsonschema import Draft6Validator
import json

def cast_value(value, value_type):
    print('cast_value type: ', value_type)
    if value_type == 'string':
        pass
    elif value_type == 'integer':
        value = int(value)
    elif value_type == 'number':
        value = float(value)
    elif value_type == 'array':
        value = json.loads(value)
    elif value_type == 'object':
        value = json.loads(value)
    return value

class JSONSchemaCLIWidget:
    def __init__(self, schema):
        self.schema = schema
        
    def render(self, on_error='raise', max_retries=10, *args, **kwargs):
        # TODO: Remove retries? delegate to UI Manager?
        failed_flag = False
        
        print('schema to get type: ', self.schema)
        
        if 'type' not in self.schema.keys():
            raise Exception('No type found in schema')
        else:
            schema_type = self.schema.get('type')
        
        value = input('Enter value(%s): ' % schema_type)
        try:
            value = cast_value(value, schema_type)
        except ValueError:
            print('error at casting value: ', value)
            failed_flag = True                
        """
        v = Draft6Validator(self.schema)
        if not v.is_valid(value):
            print('is not valid')
            failed_flag = True
        """
        if failed_flag:
            if max_retries > 0:
                max_retries -= 1  
                kwargs['max_retries'] = max_retries
                value = self.render(*args, **kwargs)
            else:
                raise ValueError('max retries')
        return value
        